import logo from "./logo.svg";
import "./App.css";
import TaiXiuPage from "./TaiXiuPage/TaiXiuPage";

function App() {
  return (
    <div className="text-center">
      <TaiXiuPage />
    </div>
  );
}

export default App;
