import React from "react";

export default function KetQua({
  luaChon,
  soLuotChoi,
  soBanThang,
  handlePlayGame,
  ketQua,
}) {
  return (
    <div>
      <button onClick={handlePlayGame} className="btn btn-warning px-5 py-3">
        Play game
      </button>
      <h1 className="my-5">{ketQua}</h1>
      {luaChon && <h2 className="my-5">Bạn chọn: {luaChon}</h2>}
      <h2 className="my-5">Số bàn thắng: {soBanThang}</h2>
      <h2>Số lượt chơi: {soLuotChoi}</h2>
    </div>
  );
}
