import React, { useState } from "react";
import bg_game from "../assets/bgGame.png";
import "./game.css";
import KetQua from "./KetQua";
import XucXac from "./XucXac";
export const TAI = "TÀI";
export const XIU = "XỈU";
export default function TaiXiuPage() {
  const [xucXacArr, setXucXacArr] = useState([
    { img: "./imgXucXac/1.png", giaTri: 1 },
    { img: "./imgXucXac/1.png", giaTri: 1 },
    { img: "./imgXucXac/1.png", giaTri: 1 },
  ]);
  const [luaChon, setLuaChon] = useState(null);
  const [soBanThang, setSoBanThang] = useState(0);
  const [soLuotChoi, setSoLuotChoi] = useState(0);
  const [ketQua, setKetQua] = useState(null);
  const handleDatCuoc = (value) => {
    setLuaChon(value);
  };
  const handlePlayGame = () => {
    let tongDiem = 0;
    let count = 0;
    let myInterval = setInterval(() => {
      count++;
      console.log("count: ", count);
      let newXucXacArr = xucXacArr.map(() => {
        let number = Math.floor(Math.random() * (6 - 1 + 1)) + 1;
        count == 3 && (tongDiem += number);
        return {
          img: `./imgXucXac/${number}.png`,
          giaTri: number,
        };
      });
      setXucXacArr(newXucXacArr);
      if (count == 3) {
        setSoLuotChoi(soLuotChoi + 1);
        tongDiem >= 11 && luaChon == TAI && setSoBanThang(soBanThang + 1);
        tongDiem < 11 && luaChon == XIU && setSoBanThang(soBanThang + 1);
        if (tongDiem >= 11 && luaChon == TAI) {
          setKetQua("Chúc mừng");
        } else if (tongDiem < 11 && luaChon == XIU) {
          setKetQua("Chúc mừng");
        } else {
          setKetQua("Chúc bạn may mắn lần sau");
        }
        clearInterval(myInterval);
      }
    }, 300);
  };

  return (
    <div
      className="game_container"
      style={{ backgroundImage: `url(${bg_game})` }}
    >
      <XucXac xucXacArr={xucXacArr} handleDatCuoc={handleDatCuoc} />
      <KetQua
        luaChon={luaChon}
        soBanThang={soBanThang}
        soLuotChoi={soLuotChoi}
        handlePlayGame={handlePlayGame}
        ketQua={ketQua}
      />
    </div>
  );
}
