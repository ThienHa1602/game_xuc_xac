import React from "react";
import { TAI, XIU } from "./TaiXiuPage";

export default function ({ xucXacArr, handleDatCuoc }) {
  const renderXucXacArr = () => {
    return xucXacArr.map((item, index) => {
      return (
        <img
          style={{ height: 100, margin: 10 }}
          src={item.img}
          alt=""
          key={index}
        />
      );
    });
  };
  return (
    <div className="p-5 d-flex justify-content-between container">
      <button
        onClick={() => {
          handleDatCuoc(TAI);
        }}
        className="btn btn-danger p-5"
      >
        Tài
      </button>
      {renderXucXacArr()}
      <button
        onClick={() => {
          handleDatCuoc(XIU);
        }}
        className="btn btn-secondary p-5"
      >
        Xỉu
      </button>
    </div>
  );
}
